/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 */
package org.eclipse.osbp.blob.component;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.osbp.ui.api.customfields.IBlobConverter;
import org.eclipse.osbp.ui.api.customfields.IBlobService;

import com.vaadin.server.StreamResource;

public class BlobConverter implements IBlobConverter {
	private String input;
	private StreamResource output;
	private IBlobService blobService;
	private int resolutionId;
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	public BlobConverter(IBlobService blobService) {
		this.blobService = blobService;
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	/**
	 * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
	 */
	public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	@Override
	public void setInput(String input) {
		this.input = input;
		if (blobService != null) {
			setOutput(blobService.getResource(input, resolutionId));
		}
	}

	@Override
	public void setOutput(StreamResource output) {
		firePropertyChange("output", this.output, this.output = output );
	}

	@Override
	public String getInput() {
		return input;
	}

	@Override
	public StreamResource getOutput() {
		return output;
	}

	@Override
	public void setBlobService(IBlobService blobService) {
		this.blobService = blobService;
	}

	@Override
	public void setResolution(int resolutionId) {
		this.resolutionId = resolutionId;
	}

	@Override
	public IBlobService getBlobService() {
		return blobService;
	}

	@Override
	public int getResolution() {
		return resolutionId;
	}
}

/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
package org.eclipse.osbp.blob;

import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class PersistenceServiceBinder.
 */
@Component
public class PersistenceServiceBinder {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("servicebinder");

	private static IPersistenceService persistenceService;

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindPersistenceService(final IPersistenceService persistenceService) {
		PersistenceServiceBinder.persistenceService = persistenceService;
		LOGGER.debug("PersistenceService bound");
	}

	public synchronized void unbindPersistenceService(final IPersistenceService persistenceService) {
		PersistenceServiceBinder.persistenceService = null;
		LOGGER.debug("PersistenceService unbound");
	}

	public static IPersistenceService getPersistenceService() {
		return persistenceService;
	}

}

package org.eclipse.osbp.blob.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import org.eclipse.osbp.blob.dtos.ContentTypeDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;

@SuppressWarnings("all")
public class MimeTypeDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  private String mimeVersion;
  
  private String contentTransferEncoding;
  
  @DomainReference
  private List<ContentTypeDto> contentTypeList;
  
  public MimeTypeDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link MimeType} to the dto {@link MimeTypeDto}.
   * 
   */
  protected void installLazyCollections() {
    
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the mimeVersion property or <code>null</code> if not present.
   */
  public String getMimeVersion() {
    return this.mimeVersion;
  }
  
  /**
   * Sets the <code>mimeVersion</code> property to this instance.
   * 
   * @param mimeVersion - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setMimeVersion(final String mimeVersion) {
    firePropertyChange("mimeVersion", this.mimeVersion, this.mimeVersion = mimeVersion );
  }
  
  /**
   * Returns the contentTransferEncoding property or <code>null</code> if not present.
   */
  public String getContentTransferEncoding() {
    return this.contentTransferEncoding;
  }
  
  /**
   * Sets the <code>contentTransferEncoding</code> property to this instance.
   * 
   * @param contentTransferEncoding - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setContentTransferEncoding(final String contentTransferEncoding) {
    firePropertyChange("contentTransferEncoding", this.contentTransferEncoding, this.contentTransferEncoding = contentTransferEncoding );
  }
  
  /**
   * Returns an unmodifiable list of contentTypeList.
   */
  public List<ContentTypeDto> getContentTypeList() {
    return Collections.unmodifiableList(internalGetContentTypeList());
  }
  
  /**
   * Returns the list of <code>ContentTypeDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<ContentTypeDto> internalGetContentTypeList() {
    if (this.contentTypeList == null) {
      this.contentTypeList = new java.util.ArrayList<ContentTypeDto>();
    }
    return this.contentTypeList;
  }
  
  /**
   * Adds the given contentTypeDto to this object. <p>
   * 
   * @param contentTypeDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToContentTypeList(final ContentTypeDto contentTypeDto) {
    checkDisposed();
    
    internalAddToContentTypeList(contentTypeDto);
  }
  
  public void removeFromContentTypeList(final ContentTypeDto contentTypeDto) {
    checkDisposed();
    
    internalRemoveFromContentTypeList(contentTypeDto);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToContentTypeList(final ContentTypeDto contentTypeDto) {
    // add this as property change listener for embeddable beans
    contentTypeDto.addPropertyChangeListener(this);
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<ContentTypeDto> oldList = null;
    		if(internalGetContentTypeList() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetContentTypeList()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetContentTypeList());
    		}
    		internalGetContentTypeList().add(contentTypeDto);
    		firePropertyChange("contentTypeList", oldList, internalGetContentTypeList());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromContentTypeList(final ContentTypeDto contentTypeDto) {
    // remove this as property change listener from the embeddable bean
    contentTypeDto.removePropertyChangeListener(this);
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<ContentTypeDto> oldList = null;
    	if(internalGetContentTypeList() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetContentTypeList()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetContentTypeList());
    	}
    	internalGetContentTypeList().remove(contentTypeDto);
    	firePropertyChange("contentTypeList", oldList, internalGetContentTypeList());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetContentTypeList().remove(contentTypeDto);
    }
  }
  
  /**
   * Sets the <code>contentTypeList</code> property to this instance.
   * 
   * @param contentTypeList - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setContentTypeList(final List<ContentTypeDto> contentTypeList) {
    checkDisposed();
    for (ContentTypeDto dto : internalGetContentTypeList().toArray(new ContentTypeDto[this.contentTypeList.size()])) {
    	removeFromContentTypeList(dto);
    }
    
    if(contentTypeList == null) {
    	return;
    }
    
    for (ContentTypeDto dto : contentTypeList) {
    	addToContentTypeList(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    
    
    	if(source == contentTypeList){
    		firePropertyChange("contentTypeList" + "_" + event.getPropertyName(), event.getOldValue(), event.getNewValue());
    	} else 
    { 
    	// no super class available to forward event
    }
  }
}

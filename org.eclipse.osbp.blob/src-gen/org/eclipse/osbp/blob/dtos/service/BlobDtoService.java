package org.eclipse.osbp.blob.dtos.service;

import org.eclipse.osbp.blob.dtos.BlobDto;
import org.eclipse.osbp.blob.entities.Blob;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;

@SuppressWarnings("all")
public class BlobDtoService extends AbstractDTOService<BlobDto, Blob> {
  public BlobDtoService() {
    // set the default persistence ID
    setPersistenceId("blob");
  }
  
  public Class<BlobDto> getDtoClass() {
    return BlobDto.class;
  }
  
  public Class<Blob> getEntityClass() {
    return Blob.class;
  }
  
  public Object getId(final BlobDto dto) {
    return dto.getId();
  }
}

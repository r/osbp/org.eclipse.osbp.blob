package org.eclipse.osbp.blob.dtos.mapper;

import org.eclipse.osbp.blob.dtos.AttributesDto;
import org.eclipse.osbp.blob.entities.Attributes;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link AttributesDto} to and from the entity {@link Attributes}.
 * 
 */
@SuppressWarnings("all")
public class AttributesDtoMapper<DTO extends AttributesDto, ENTITY extends Attributes> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public Attributes createEntity() {
    return new Attributes();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public AttributesDto createDto() {
    return new AttributesDto();
  }
  
  /**
   * Maps the entity {@link Attributes} to the dto {@link AttributesDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final AttributesDto dto, final Attributes entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    dto.setSize(toDto_size(entity, context));
    dto.setResolution(toDto_resolution(entity, context));
  }
  
  /**
   * Maps the dto {@link AttributesDto} to the entity {@link Attributes}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final AttributesDto dto, final Attributes entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    
    entity.setSize(toEntity_size(dto, entity, context));
    entity.setResolution(toEntity_resolution(dto, entity, context));
  }
  
  /**
   * Maps the property size from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_size(final Attributes in, final MappingContext context) {
    return in.getSize();
  }
  
  /**
   * Maps the property size from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_size(final AttributesDto in, final Attributes parentEntity, final MappingContext context) {
    return in.getSize();
  }
  
  /**
   * Maps the property resolution from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_resolution(final Attributes in, final MappingContext context) {
    return in.getResolution();
  }
  
  /**
   * Maps the property resolution from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_resolution(final AttributesDto in, final Attributes parentEntity, final MappingContext context) {
    return in.getResolution();
  }
  
  public String createDtoHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
  
  public String createEntityHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
}

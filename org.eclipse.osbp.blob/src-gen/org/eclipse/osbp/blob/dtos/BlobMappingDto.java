package org.eclipse.osbp.blob.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.eclipse.osbp.blob.dtos.BlobDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Id;

@SuppressWarnings("all")
public class BlobMappingDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Id
  private String id = java.util.UUID.randomUUID().toString();
  
  private String uniqueName;
  
  private String fileName;
  
  private int mimeTypeId;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<BlobDto> blobsRef;
  
  public BlobMappingDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link BlobMapping} to the dto {@link BlobMappingDto}.
   * 
   */
  protected void installLazyCollections() {
    blobsRef = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				BlobDto.class, "blobMapping.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    return this.id;
  }
  
  /**
   * Sets the <code>id</code> property to this instance.
   * 
   * @param id - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setId(final String id) {
    firePropertyChange("id", this.id, this.id = id );
    				installLazyCollections();
  }
  
  /**
   * Returns the uniqueName property or <code>null</code> if not present.
   */
  public String getUniqueName() {
    return this.uniqueName;
  }
  
  /**
   * Sets the <code>uniqueName</code> property to this instance.
   * 
   * @param uniqueName - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUniqueName(final String uniqueName) {
    firePropertyChange("uniqueName", this.uniqueName, this.uniqueName = uniqueName );
  }
  
  /**
   * Returns the fileName property or <code>null</code> if not present.
   */
  public String getFileName() {
    return this.fileName;
  }
  
  /**
   * Sets the <code>fileName</code> property to this instance.
   * 
   * @param fileName - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFileName(final String fileName) {
    firePropertyChange("fileName", this.fileName, this.fileName = fileName );
  }
  
  /**
   * Returns the mimeTypeId property or <code>null</code> if not present.
   */
  public int getMimeTypeId() {
    return this.mimeTypeId;
  }
  
  /**
   * Sets the <code>mimeTypeId</code> property to this instance.
   * 
   * @param mimeTypeId - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setMimeTypeId(final int mimeTypeId) {
    firePropertyChange("mimeTypeId", this.mimeTypeId, this.mimeTypeId = mimeTypeId );
  }
  
  /**
   * Returns an unmodifiable list of blobsRef.
   */
  public List<BlobDto> getBlobsRef() {
    return Collections.unmodifiableList(internalGetBlobsRef());
  }
  
  /**
   * Returns the list of <code>BlobDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<BlobDto> internalGetBlobsRef() {
    if (this.blobsRef == null) {
      this.blobsRef = new java.util.ArrayList<BlobDto>();
    }
    return this.blobsRef;
  }
  
  /**
   * Adds the given blobDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>BlobDto#blobMapping</code> of the <code>blobDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link BlobDto#setBlobMapping(BlobDto)}.
   * 
   * @param blobDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToBlobsRef(final BlobDto blobDto) {
    checkDisposed();
    
    blobDto.setBlobMapping(this);
  }
  
  /**
   * Removes the given blobDto from this object. <p>
   * 
   * @param blobDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromBlobsRef(final BlobDto blobDto) {
    checkDisposed();
    
    blobDto.setBlobMapping(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToBlobsRef(final BlobDto blobDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<BlobDto> oldList = null;
    		if(internalGetBlobsRef() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetBlobsRef()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetBlobsRef());
    		}
    		internalGetBlobsRef().add(blobDto);
    		firePropertyChange("blobsRef", oldList, internalGetBlobsRef());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromBlobsRef(final BlobDto blobDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<BlobDto> oldList = null;
    	if(internalGetBlobsRef() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetBlobsRef()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetBlobsRef());
    	}
    	internalGetBlobsRef().remove(blobDto);
    	firePropertyChange("blobsRef", oldList, internalGetBlobsRef());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetBlobsRef().remove(blobDto);
    }
  }
  
  /**
   * Sets the <code>blobsRef</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>BlobDto#
   * blobMapping</code> of the <code>blobsRef</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link BlobDto#setBlobMapping(BlobDto)
   * 
   * @param blobsRef - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setBlobsRef(final List<BlobDto> blobsRef) {
    checkDisposed();
    for (BlobDto dto : internalGetBlobsRef().toArray(new BlobDto[this.blobsRef.size()])) {
    	removeFromBlobsRef(dto);
    }
    
    if(blobsRef == null) {
    	return;
    }
    
    for (BlobDto dto : blobsRef) {
    	addToBlobsRef(dto);
    }
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BlobMappingDto other = (BlobMappingDto) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	// no super class available to forward event
    }
  }
}

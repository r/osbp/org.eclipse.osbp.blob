package org.eclipse.osbp.blob.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.osbp.blob.entities.Blob;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "BLOB_MAPPING")
@DiscriminatorValue(value = "BLOB_MAPPING")
@SuppressWarnings("all")
public class BlobMapping implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "ID")
  private String id = java.util.UUID.randomUUID().toString();
  
  @Column(name = "UNIQUE_NAME")
  private String uniqueName;
  
  @Column(name = "FILE_NAME")
  private String fileName;
  
  @Column(name = "MIME_TYPE_ID")
  private int mimeTypeId;
  
  @JoinColumn(name = "BLOBS_REF_ID")
  @OneToMany(mappedBy = "blobMapping", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Blob> blobsRef;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.blobsRef != null) {
        for (Blob blob : this.blobsRef) {
          blob.dispose();
        }
        this.blobsRef = null;
      }
      
    }
    finally {
      disposed = true;
    }
    
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final String id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the uniqueName property or <code>null</code> if not present.
   */
  public String getUniqueName() {
    checkDisposed();
    return this.uniqueName;
  }
  
  /**
   * Sets the uniqueName property to this instance.
   */
  public void setUniqueName(final String uniqueName) {
    checkDisposed();
    this.uniqueName = uniqueName;
  }
  
  /**
   * @return Returns the fileName property or <code>null</code> if not present.
   */
  public String getFileName() {
    checkDisposed();
    return this.fileName;
  }
  
  /**
   * Sets the fileName property to this instance.
   */
  public void setFileName(final String fileName) {
    checkDisposed();
    this.fileName = fileName;
  }
  
  /**
   * @return Returns the mimeTypeId property or <code>null</code> if not present.
   */
  public int getMimeTypeId() {
    checkDisposed();
    return this.mimeTypeId;
  }
  
  /**
   * Sets the mimeTypeId property to this instance.
   */
  public void setMimeTypeId(final int mimeTypeId) {
    checkDisposed();
    this.mimeTypeId = mimeTypeId;
  }
  
  /**
   * @return Returns an unmodifiable list of blobsRef.
   */
  public List<Blob> getBlobsRef() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetBlobsRef());
  }
  
  /**
   * Sets the given blobsRef to the object. Currently contained blobsRef instances will be removed.
   * 
   * @param blobsRef the list of new instances
   */
  public void setBlobsRef(final List<Blob> blobsRef) {
    // remove the old blob
    for(Blob oldElement : new ArrayList<Blob>(this.internalGetBlobsRef())){
      removeFromBlobsRef(oldElement);
    }
    
    // add the new blob
    for(Blob newElement : blobsRef){
      addToBlobsRef(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Blob</code>s thereby lazy initializing it.
   */
  public List<Blob> internalGetBlobsRef() {
    if (this.blobsRef == null) {
      this.blobsRef = new ArrayList<Blob>();
    }
    return this.blobsRef;
  }
  
  /**
   * Adds the given blob to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Blob.blobMapping)
   * of the blob will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Blob#setBlobMapping(Blob)}.
   * 
   */
  public void addToBlobsRef(final Blob blob) {
    checkDisposed();
    blob.setBlobMapping(this);
  }
  
  /**
   * Removes the given blob from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Blob.blobMapping)
   * of the blob will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Blob#setBlobMapping(Blob)}.
   * 
   */
  public void removeFromBlobsRef(final Blob blob) {
    checkDisposed();
    blob.setBlobMapping(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToBlobsRef(final Blob blob) {
    if(blob == null) {
    	return;
    }
    
    		internalGetBlobsRef().add(blob);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromBlobsRef(final Blob blob) {
    internalGetBlobsRef().remove(blob);
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BlobMapping other = (BlobMapping) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}

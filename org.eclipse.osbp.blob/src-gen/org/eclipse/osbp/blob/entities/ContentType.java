package org.eclipse.osbp.blob.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
@SuppressWarnings("all")
public class ContentType implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @XmlAttribute
  @Basic
  private int id;
  
  @XmlAttribute
  @Basic
  private String type;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public int getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final int id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the type property or <code>null</code> if not present.
   */
  public String getType() {
    checkDisposed();
    return this.type;
  }
  
  /**
   * Sets the type property to this instance.
   */
  public void setType(final String type) {
    checkDisposed();
    this.type = type;
  }
}
